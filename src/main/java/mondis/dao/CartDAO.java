package mondis.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import mondis.model.Purchase;
import mondis.connection.ConnectionManager;
import mondis.model.Cart;

public class CartDAO {

	private static ArrayList<Purchase> cartItems = new ArrayList<Purchase>();
	private double purchaseTotal;
	private double eachamount;
	static Connection currentCon = null;
	static ResultSet rs = null; 
	static PreparedStatement ps1=null, ps2=null,ps3=null;
	static Statement stmt=null;
	static String email, address, product_id, date, purchasestatus, cust_id;
	static int purchaseid;
	int quantity;
	double totalamount;
	
	public int getLineItemCount() {
		return cartItems.size();
	}
	
	//TO DELETE CART LIST IN THE CART.JSP
	public void deleteCartItem(String itemIndex) {
		int strItemIndex=0;;
		try {
				strItemIndex=Integer.parseInt(itemIndex);
				cartItems.remove(strItemIndex-1);
				calculatePurchaseTotal();
		} catch(NumberFormatException nfe) {
				System.out.println("Error while deleting cart item!"+nfe.getMessage());
				nfe.printStackTrace();
		}
	}
	
	//TO UPDATE QUANTITY CART LIST IN THE CART.JSP
		public void updateCartItem(String ItemIndex, String quantity) {	
			try {
				double totalCost=0.0;
				double unitCost=0.0;
				int qtty=0;
				int strItemIndex=0;
				Purchase purchaseItem= null;
				
				try {
					strItemIndex=Integer.parseInt(ItemIndex);
					qtty=Integer.parseInt(quantity);
					if(qtty>0) {
						purchaseItem=(Purchase)cartItems.get(strItemIndex-1);
						unitCost =purchaseItem.getUnitPrice();
						totalCost=unitCost*qtty;
						purchaseItem.setOrderQtty(qtty);
						purchaseItem.setTotalamount(totalCost);
						calculatePurchaseTotal();
					}
				}catch (NumberFormatException nfe) {
					System.out.println("Error while updating cart:"+nfe.getMessage());
					nfe.printStackTrace();
				}
			}catch (NumberFormatException nfe) {
				System.out.println("Error while updating cart:"+nfe.getMessage());
				nfe.printStackTrace();
			}
		}
		
		//TO ADD food INSIDE THE ARRAY LIST 
		public void addCartItem(String product_id, String product_name, String prodPrice, String quantity) {
			double totalCost=0.0;
			double unitCost=0.0;
			int qtty=0;
			Purchase purchaseItem = new Purchase();
			
			try {
				unitCost= Double.parseDouble(prodPrice);
				qtty=Integer.parseInt(quantity);
				if(qtty>0) {
					totalCost= unitCost*qtty;
					purchaseItem.setproduct_id(product_id);
					purchaseItem.setProduct_name(product_name);
					purchaseItem.setUnitPrice(unitCost);
					purchaseItem.setOrderQtty(qtty);
					purchaseItem.setTotalamount(totalCost);
					cartItems.add(purchaseItem);
					calculatePurchaseTotal();
					System.out.println("testttttttttttttttttt"+cartItems);
				}
			} catch(NumberFormatException nfe) {
				System.out.println("Error while parsing from String to primitive types:"+nfe.getMessage());
				nfe.printStackTrace();
			}
		}
		
		public void addCartItem(Purchase purchaseItem) {
			cartItems.add(purchaseItem);
		}
		
		public Purchase getCartItem(int strproduct_id) {
			Purchase purchaseItem = null;
			if(cartItems.size()>strproduct_id) {
				purchaseItem=(Purchase) cartItems.get(strproduct_id);
			}
			return purchaseItem;
		}
		
		public ArrayList<Purchase> getCartItems() {
			return cartItems;
		}
		public void setCartItems(ArrayList<Purchase> cartItems) {
			this.cartItems= cartItems;
		}
		
		public double getEachamount() {
			return eachamount;
		}
		
		public void setEachamount(double eachamount) {
			this.eachamount=eachamount;
		}
		
		public double getPurchaseTotal() {
			return purchaseTotal;
		}
		
		public void setPurchaseTotal(double purchaseTotal) {
			this.purchaseTotal=purchaseTotal;
		}
		
		//TO CALCULATE TOTAL PURCHASE 
		protected void calculatePurchaseTotal() {
			double total=0;
			for(int counter=0;counter<cartItems.size();counter++) {
				Purchase purchaseItem=(Purchase) cartItems.get(counter);
				total+=purchaseItem.getTotalamount();
			}
			setPurchaseTotal(total);
		}
		
		public void add(String cust_id, String date, String address, String product_id, int quantity, Double ta, String ItemIndex) 
		{
			Cart cart = new Cart();
			Purchase purchaseItem= null;

			
			cart.setProduct_id(product_id);
			cart.setOrderQtty(quantity);
			cart.setTotalamount(ta);
			cart.setDate(date);
			cart.setAddress(address);
			cart.setCust_email(cust_id);
	        
			int strItemIndex=0;
			strItemIndex=Integer.parseInt(ItemIndex);
			
			
	        System.out.print(product_id+ quantity+ address+ date + cust_id+ ta + purchasestatus);
	        
	      
			try 
	    	{		//inserting the ORDER TABLE
		    		currentCon = ConnectionManager.getConnection();
		    		ps1=currentCon.prepareStatement("insert into orders (order_totalamount, order_date, order_address, cust_id)values(?,to_date(?,'yyyy-mm-dd'),?,?)");
		    		ps1.setDouble(1,ta);
		    		ps1.setString(2,date);
		    		ps1.setString(3,address);
		    		ps1.setString(4,cust_id);
		    		ps1.executeUpdate();
		    		System.out.print("You have inserted the order");
	    		
		    	//TO GET ORDER_ID
	    		ps2=currentCon.prepareStatement("select * from purchases where cust_id=? and to_char(order_date,'yyyy-mm-dd')=?");
	    		ps2.setString(1,cust_id);
	    		ps2.setString(2,date);
	    		ResultSet rs = ps2.executeQuery();
	    		System.out.print("You have search....");
	            
	            if (rs.next()) 
	            {
	            	cart.setPurchaseid(rs.getInt("purchase_id"));
	            }
	    		
	            purchaseid = cart.getPurchaseid();
			    System.out.print("BETA" + purchaseid);
	           
			    //TO INSERT DATA INTO ORDERITEMS TABLE DATABASE
	    		ps3=currentCon.prepareStatement("insert into purchaseitem (purchase_id, product_id, quantity)values(?,?,?)");

	    		
	    		System.out.print("BRAVO");
	    		for (Purchase items : cartItems) {
	    			for(int counter=0; counter<cartItems.size();counter++)
	    			{
	    				ps3.setInt(1, purchaseid);
	        			System.out.print(purchaseid);
	    			}
		    			ps3.setString(2,items.getProduct_id());
		    			
		    			ps3.setInt(3, items.getOrderQtty());
		    			
		    			System.out.print("You have inserted the orderitem");		
		    			ps3.executeUpdate();
	    		}
	    	}

	    	catch (Exception ex) 
	    	{
	    		System.out.println("failed: An Exception has occurred! " + ex);
	    	}

		}

}
