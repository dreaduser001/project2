package mondis.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import mondis.connection.ConnectionManager;
import mondis.model.Admin;

public class AdminDAO {

	static Connection currentCon = null;
	static ResultSet rs = null;
	
	public static Admin login(Admin admin) { 
		// preparing some objects for connection 
		System.out.println("Testing...."); 
		Statement stmt = null;
		
		String admin_email = admin.getAdmin_email(); 
		String admin_password = admin.getAdmin_password(); 
		
		String searchQuery = "select * from admin where admin_email='" + admin_email + "' AND admin_password='" + admin_password + "'";
		
		// "System.out.println" prints in the console; Used to trace the process 
		System.out.println("Your Email is " + admin_email); 
		System.out.println("Your password is " + admin_password); 
		System.out.println("Query: " + searchQuery); 
		
		try { 
			// connect to DB 
			currentCon = ConnectionManager.getConnection(); 
			stmt = currentCon.createStatement(); 
			rs = stmt.executeQuery(searchQuery); 
			boolean more = rs.next(); 
			
			// if user does not exist set the isValid variable to false 
			if (!more) { 
				System.out.println("Sorry, you are not admin! Please go to the Customer login section"); 
				admin.setValid(false); 
				} 
			
			// if user exists set the isValid variable to true 
			else if (more) { 
				String admin_name = rs.getString("admin_name");
 
				
				System.out.println("Welcome " + admin_name); 
				admin.setAdmin_name(admin_name); 
				admin.setValid(true); 
				} 
			} 
		
		catch (Exception ex) { 
			System.out.println("Log In failed: An Exception has occurred! " + ex); 
			} 
		
		// some exception handling 
		finally { 
			if (rs != null) { 
				try { 
					rs.close(); 
					} catch (Exception e) {
					} 
				rs = null; 
				} 
			
			if (stmt != null) { 
				try { 
					stmt.close(); 
					} catch (Exception e) { 
					} 
				stmt = null; 
				}
			
			if (currentCon != null) { 
				try { 
					currentCon.close(); 
					} catch (Exception e) { 
					} 
				
				currentCon = null; 
				} 
			} 
		return admin; 
		}
}
