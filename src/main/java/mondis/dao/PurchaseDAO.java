package mondis.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import mondis.model.Purchase;
import mondis.connection.ConnectionManager;

public class PurchaseDAO {

	private static ArrayList<Purchase> purchases = new ArrayList<Purchase>();
	static Connection currentCon = null;
	static ResultSet rs = null; 
	static PreparedStatement ps1=null, ps2=null;
	static Statement stmt=null, stmt2= null;
	static String  name, ptype, ftype, stype, description, email;
	static double price;
	static int id;
	
	//VIEW LIST PURCHASES FOR CUSTOMERS
		public List<Purchase> viewPurchaseByCust(String cust_id) throws IOException 
		{
			
			List<Purchase> purchases = new ArrayList<Purchase>();
			  
			  try 
			  {
				  
			  	currentCon = ConnectionManager.getConnection();
			  	stmt = currentCon.createStatement();
			  
			  	  String q = "select * from purchases where cust_id='" + cust_id +"'";
			      ResultSet rs = stmt.executeQuery(q);
			      
			      while (rs.next()) 
			      {
			    	  Purchase purchase = new Purchase();
			    	  purchase.setPurchaseid(rs.getInt("purchase_id"));
			    	  purchase.setDate(rs.getString("purchase_date"));
			         
			    	  purchases.add(purchase);
			      }
			  } 
			  catch (SQLException e) 
			  {
			      e.printStackTrace();
			  }

			  return purchases;
		}
		
		//VIEW PURCHASES BY ADMIN
		
		public List<Purchase> viewAllCust() throws IOException 
		{
			
			List<Purchase> purchases = new ArrayList<Purchase>();
			  
			  try 
			  {
				  
			  	currentCon = ConnectionManager.getConnection();
			  	stmt = currentCon.createStatement();
			  
			  	  String q = "select * from purchases";
			      ResultSet rs = stmt.executeQuery(q);
			      
			      while (rs.next()) 
			      {
			    	  Purchase order = new Purchase();
			    	  order.setPurchaseid(rs.getInt("purchase_id"));
			    	  order.setDate(rs.getString("purchase_date"));
			         
			    	  purchases.add(order);
			      }
			  } 
			  catch (SQLException e) 
			  {
			      e.printStackTrace();
			  }

			  return purchases;
		}
		public void deletePurchase(int purchase_id) {
			String searchQuery1 = "delete from purchases where purchase_id = " + "'" + purchase_id + "'";
			String searchQuery2 = "delete from purchaseitem where purchase_id = " + "'" + purchase_id + "'";
			System.out.println(searchQuery1);
			System.out.println(searchQuery2);
			
			try 
			{
		        currentCon = ConnectionManager.getConnection();
		        stmt = currentCon.createStatement();
		        stmt.executeUpdate(searchQuery1); 
		        stmt.executeUpdate(searchQuery2);
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
		
		}
		
		public Purchase getViewPurchase(int purchase_id) {
			Purchase purchase = new Purchase();
			System.out.print("purchase_id");
			try {

			  	currentCon = ConnectionManager.getConnection();
				PreparedStatement ps = currentCon.prepareStatement("select * from customer join purchases using(cust_id) where purchase_id=?");

				ps.setInt(1, purchase_id);
				ResultSet rs = ps.executeQuery();
			  
			  	
			  	 while (rs.next()) 
			      {
			  		purchase.setPurchaseid(rs.getInt("purchase_id"));
			  		purchase.setCust_email(rs.getString("cust_id"));	
			  		purchase.setCust_name(rs.getString("cust_name"));	
			  		purchase.setDate(rs.getString("purchase_date"));	
			  		purchase.setPhonenumber(rs.getString("cust_phone"));	
			  		purchase.setAddress(rs.getString("purchase_address"));	
			  		purchase.setTotalamount(Double.parseDouble(rs.getString("purchase_totalamount")));	
			      }
			  	
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
			return purchase;
		
		}
		
		public List<Purchase> viewlistofeachpurchase(int purchaseid) throws IOException 
		{
			System.out.print("Testing view " +purchaseid);
			List<Purchase> purchases = new ArrayList<Purchase>();
			  
			  try 
			  {
			  	currentCon = ConnectionManager.getConnection();
			  	stmt = currentCon.createStatement();
			  		double totalcost=0;
			  	  String q = "select * from customer join purchases using(cust_email) join purchaseitem using (purchase_id) join product using (product_id) where purchase_id="+purchaseid;
			      ResultSet rs = stmt.executeQuery(q);
			      
			      while (rs.next()) 
			      {
			    	  Purchase purchase = new Purchase();
			    	  purchase.setPurchaseid(rs.getInt("purchase_id"));
			    	  purchase.setproduct_id(rs.getString("product_id"));	
			    	  purchase.setProduct_name(rs.getString("product_name"));	
			    	  purchase.setOrderQtty(rs.getInt("quantity"));	
			    	  purchase.setUnitPrice(rs.getDouble("product_unitprice"));	
			    	  totalcost=( purchase.getOrderQtty()*purchase.getUnitPrice());
			    	  purchase.setTotalamount(totalcost);
			    	  purchases.add(purchase);
			      }
			  } 
			  catch (SQLException e) 
			  {
			      e.printStackTrace();
			  }

			  return purchases;
		}
		
		public void updatePurchase(int purchase_id, String date, String address, ArrayList<Purchase> orders) {
			
			double totalcost = 0.00;
			double price=0.00;
			System.out.print("\nPurchase ID: "+purchase_id+"\n");
			System.out.print("Date: "+date+"\n");
			System.out.print("Address: "+address+"\n");
			System.out.print("testing....");
			
			String searchQuery1 = "UPDATE purchases SET purchase_date = to_date('"+ date +"','yyyy-mm-dd'), purchase_address = '"+ address +"' where purchase_id='"+purchase_id+"'";
			
		
			
			
			try {
				currentCon = ConnectionManager.getConnection();
			  	PreparedStatement stmt1 = currentCon.prepareStatement("select * from purchases where purchase_id="+purchase_id);
			  	PreparedStatement stmt2 = currentCon.prepareStatement("select * from purchaseitem where purchase_id="+purchase_id);
			  	PreparedStatement stmt3 = currentCon.prepareStatement("select * from purchases where purchase_id="+purchase_id);
			  	stmt1.executeUpdate(searchQuery1);
			  	for(Purchase list : orders)
				{
			  		
					String product = list.getProduct_id();
					int quantity = Integer.parseInt(list.getStringQuantity());
					price = list.getUnitPrice();
					
					System.out.print("\nID Produk: "+ product+ "\nJumlah Produk: " +quantity+"\n");
					String searchQuery2 = "UPDATE purchaseitem SET quantity ="+quantity+" where purchase_id = '"+purchase_id+"' and product_id='"+product+"'";
					stmt2.executeUpdate(searchQuery2);
					 totalcost+= (quantity * price);
					System.out.print("\nJUMLAH KESEMUA PRODUK:OK " + totalcost);

				}
			  	String searchQuery3 = "UPDATE purchases SET purchase_totalamount = '"+ totalcost +"' where purchase_id='"+purchase_id+"'";
			  	stmt3.executeUpdate(searchQuery3);
			  	System.out.print("\nJUMLAH KESEMUA PRODUK: " + totalcost);
				
			  	
			  	
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
		
		}
}
