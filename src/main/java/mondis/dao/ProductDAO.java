package mondis.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import mondis.model.Product;
import mondis.connection.ConnectionManager;

public class ProductDAO {

	static Connection currentCon = null;
	static ResultSet rs = null; 
	static PreparedStatement ps=null;
	static Statement stmt=null;
	static String id, name, type, description, email;
	static double price;
	
	public Product getProduct(Product product)
	{
		id = product.getProduct_id();
		String searchQuery = "select * from product where product_id='" + id + "'";

        try 
        {
            currentCon = ConnectionManager.getConnection();
            stmt = currentCon.createStatement();
            rs = stmt.executeQuery(searchQuery);
            boolean more = rs.next();
            
            System.out.println(searchQuery);

            // if subject exists set the isValid variable to true
            if (more) 
            {
                product.setValid(true);
           	}
           
            else if (!more) 
            {            	
            	product.setValid(false);
            }
           
        }

        catch (Exception ex) 
        {
            System.out.println("Log In failed: An Exception has occurred! " + ex);
        }

        finally 
        {
            if (rs != null) 
            {
                try 
                {
                    rs.close();
                } 
                catch (Exception e) {}
                rs = null;
            }

            if (stmt != null) 
            {
                try 
                {
                    stmt.close();
                } 
                catch (Exception e) {}
                stmt = null;
            }

            if (currentCon != null) 
            {
                try 
                {
                    currentCon.close();
                } 
                catch (Exception e) {}

                currentCon = null;
            }
        }

        return product;
	}
	
	public void add(Product product, String email) 
	{
		
        id = product.getProduct_id();
        name = product.getProduct_name();
        price = product.getProduct_unitprice();
        type = product.getProduct_type();
 
    	try 
    	{
    		currentCon = ConnectionManager.getConnection();
    		System.out.print("Testing add");
    		ps=currentCon.prepareStatement("insert into product (product_id, product_name, product_type,email, product_unitprice)values(?,?,?,?,?)");
    		System.out.print("Add Ok");
    		
    		ps.setString(1,id);
    		ps.setString(2,name);
    		ps.setString(3,type);
    		ps.setString(4,email);
            ps.setDouble(5,price);
    		ps.executeUpdate();
    		
    		System.out.println("Id is " + id);
			System.out.println("Name is " + name);
    		System.out.println("Price is " + price);
    		System.out.println("Menu Type is " + type);
    	}

    	catch (Exception ex) 
    	{
    		System.out.println("failed: An Exception has occurred! " + ex);
    	}

    	finally 
    	{
    		if (ps != null) 
    		{
    			try 
    			{
    				ps.close();
    			} catch (Exception e) {}
    			ps = null;
    		}
    		
    		if (currentCon != null) 
    		{
    			try 
    			{
    				currentCon.close();
    			} catch (Exception e) {}
    			currentCon = null;
    		}
    	}
	}
	
	public List<Product> getAllSubscription() throws IOException 
	{
		List<Product> subscription = new ArrayList<Product>();
		  
		  try 
		  {
		  	currentCon = ConnectionManager.getConnection();
		  	stmt = currentCon.createStatement();
		  
		  	  String q = "select * from product where product_type = 'SUBSCRIPTION'";
		      ResultSet rs = stmt.executeQuery(q);
		      
		      while (rs.next()) 
		      {
		    	  Product product = new Product();		          
		          
		    	  product.setProduct_id(rs.getString("product_id"));
		    	  product.setProduct_name(rs.getString("product_name"));
		    	  product.setProduct_type(rs.getString("product_type"));
		    	  product.setProduct_unitprice(rs.getDouble("product_unitprice"));
		          
		    	  subscription.add(product);
		    	  System.out.println(subscription);
		      }
		  } 
		  catch (SQLException e) 
		  {
		      e.printStackTrace();
		  }

		  return subscription;
	}
	
	public List<Product> getAllChannel() throws IOException 
	{
		List<Product> channel = new ArrayList<Product>();
		  
		  try 
		  {
		  	currentCon = ConnectionManager.getConnection();
		  	stmt = currentCon.createStatement();
		  
		  	  String q = "select * from product where product_type = 'CHANNEL'";
		      ResultSet rs = stmt.executeQuery(q);
		      
		      while (rs.next()) 
		      {
		    	  Product product = new Product();		          
		          
		          product.setProduct_id(rs.getString("product_id"));
		          product.setProduct_name(rs.getString("product_name"));
		          product.setProduct_type(rs.getString("product_type"));
		          product.setProduct_unitprice(rs.getDouble("product_unitprice"));
		          
		          channel.add(product);
		      }
		  } 
		  catch (SQLException e) 
		  {
		      e.printStackTrace();
		  }

		  return channel;
	}
	
	public List<Product> getAllServices() throws IOException 
	{
		List<Product> services = new ArrayList<Product>();
		  
		  try 
		  {
		  	currentCon = ConnectionManager.getConnection();
		  	stmt = currentCon.createStatement();
		  
		  	  String q = "select * from product where product_type = 'SERVICE'";
		      ResultSet rs = stmt.executeQuery(q);
		      
		      while (rs.next()) 
		      {
		    	  Product product = new Product();		          
		          
		          product.setProduct_id(rs.getString("product_id"));
		          product.setProduct_name(rs.getString("product_name"));
		          product.setProduct_type(rs.getString("product_type"));
		          product.setProduct_unitprice(rs.getDouble("product_unitPrice"));
		          
		          services.add(product);
		      }
		  } 
		  catch (SQLException e) 
		  {
		      e.printStackTrace();
		  }

		  return services;
	}
	
	public Product getProductByProduct_id(String id) throws IOException
	{
		Product product = new Product();

        try 
        {
            currentCon = ConnectionManager.getConnection();
            ps = currentCon.prepareStatement("select * from product where product_id = ?");
            
            ps.setString(1, id);
            ResultSet rs = ps.executeQuery();
            
            if (rs.next()) 
            {
            	product.setProduct_id(rs.getString("product_id"));
            	product.setProduct_name(rs.getString("product_name"));
            	product.setProduct_unitprice(rs.getDouble("product_unitPrice"));
            	product.setProduct_type(rs.getString("product_type"));
           	}
        }

        catch (SQLException e) 
        {
        	e.printStackTrace();
        }

        return product;
	}
	
	public void updateProduct(Product product) 
	{
		id = product.getProduct_id();
        name = product.getProduct_name();
        price = product.getProduct_unitprice();
        type = product.getProduct_type();
        
        String searchQuery = "UPDATE product SET product_name = '" + name + "', product_unitPrice = '" + price + "' WHERE product_id = '" + id + "'";
        
        try
        {
        	currentCon = ConnectionManager.getConnection();
	        stmt = currentCon.createStatement();
	        stmt.executeUpdate(searchQuery); 
        }
        
        catch (SQLException e) 
        {
        	e.printStackTrace();
        }
	}
	
	public void deleteProduct(String product_id) 
	{
		String searchQuery = "delete from product where product_id = " + "'" + product_id + "'";
		System.out.println(searchQuery);
		
		try 
		{
	        currentCon = ConnectionManager.getConnection();
	        stmt = currentCon.createStatement();
	        stmt.executeUpdate(searchQuery); 
	    } 
		
		catch (SQLException e) 
		{
	        e.printStackTrace();
	    }
	}
}
