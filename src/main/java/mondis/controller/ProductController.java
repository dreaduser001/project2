package mondis.controller;

import java.io.IOException;
import java.io.InputStream;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import mondis.dao.ProductDAO;
import mondis.model.Product;

/**
 * Servlet implementation class ProductController
 */
@WebServlet("/ProductController")
public class ProductController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static String MAIN_PAGE = "adminIndex.html";
	private static String LIST_PRODUCT = "shop.jsp";
	private static String LIST_ADMIN_PRODUCT = "adminShop.jsp";
	private static String LIST_CUSTOMER_PRODUCT = "customerShop.jsp";
	private static String LIST_ORDER_PRODUCT = "purchase.jsp";
	private static String UPDATE_PRODUCT = "updateProduct.jsp";
	
	private ProductDAO daoProduct;
	
	String forward="";
    /**
     * Default constructor. 
     */
    public ProductController() {
        daoProduct = new ProductDAO();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
String action = request.getParameter("action");
		
		if (action.equalsIgnoreCase("listProduct")) 
		{
			forward = LIST_PRODUCT;
			request.setAttribute("subscription", daoProduct.getAllSubscription());	
            request.setAttribute("channel", daoProduct.getAllChannel());
            request.setAttribute("services", daoProduct.getAllServices());
		}
		
		else if (action.equalsIgnoreCase("listAdminProduct")) 
		{
			forward = LIST_ADMIN_PRODUCT;
			request.setAttribute("subscription", daoProduct.getAllSubscription());	
            request.setAttribute("channel", daoProduct.getAllChannel());
            request.setAttribute("services", daoProduct.getAllServices());
		}
		
		else if (action.equalsIgnoreCase("listCustomerProduct")) 
		{
			forward = LIST_CUSTOMER_PRODUCT;
            request.setAttribute("subscription", daoProduct.getAllSubscription());	
            request.setAttribute("channel", daoProduct.getAllChannel());
            request.setAttribute("services", daoProduct.getAllServices());
		}
		
		else if (action.equalsIgnoreCase("listOrderProduct")) 
		{
			forward = LIST_ORDER_PRODUCT;
            request.setAttribute("subscription", daoProduct.getAllSubscription());	
            request.setAttribute("channel", daoProduct.getAllChannel());
            request.setAttribute("services", daoProduct.getAllServices());
		}
		
		else if (action.equalsIgnoreCase("updateProduct"))
		{
			String product_id = request.getParameter("product_id");
			Product product = new Product();
			product = daoProduct.getProductByProduct_id(product_id);
			forward = UPDATE_PRODUCT;
			request.setAttribute("product", product);
		}
		
		else if (action.equalsIgnoreCase("deleteProduct"))
		{
			String product_id = request.getParameter("product_id");
			daoProduct.deleteProduct(product_id);
			forward = LIST_ADMIN_PRODUCT;
			request.setAttribute("subscription", daoProduct.getAllSubscription());	
            request.setAttribute("channel", daoProduct.getAllChannel());
            request.setAttribute("services", daoProduct.getAllServices());
		}
		
		else 
		{
			forward = MAIN_PAGE;
		}
		
		
		RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String email =(String) session.getAttribute("currentSessionAdmin");
		System.out.print(email);
		String product_id = request.getParameter("product_id");
		String product_name = request.getParameter("product_name");
		String product_type = request.getParameter("product_type");
		double product_unitPrice = Double.parseDouble(request.getParameter("product_unitprice"));
		
		
        
		Product product = new Product(product_id, product_name, product_type, email, product_unitPrice);
		
		product = daoProduct.getProduct(product);
	
		if(!product.isValid())
		{			
			System.out.println("inserting product");   	
			daoProduct.add(product, email);
        	response.sendRedirect("ProductController?action=listAdminProduct");
        }
		
		else
		{
        	System.out.println("Menu already exist");        	        	
        	daoProduct.updateProduct(product);

			request.setAttribute("product", daoProduct.getProductByProduct_id(product_id));
			response.sendRedirect("ProductController?action=listAdminProduct");
        }
	}

}
