package mondis.controller;

import java.io.IOException;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import mondis.dao.CustomerDAO;
import mondis.model.Customer;

/**
 * Servlet implementation class AccountController
 */
@WebServlet("/AccountController")
public class AccountController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private CustomerDAO dao;
	private static String VIEW_ACCOUNT = "account.jsp";
	private static String VIEW_CUSTOMERACCOUNT = "customeraccount.jsp";
	private static String UPDATE_ACCOUNT = "accountupdate.jsp";
	private static String MAIN_PAGE = "customerindex.jsp";
    /**
     * Default constructor. 
     */
    public AccountController() {
    	dao = new CustomerDAO();
    }

    String forward;
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Customer cust = new Customer();
		String action = request.getParameter("action");
		HttpSession session = request.getSession();
		String email =(String) session.getAttribute("currentSessionUser");
		
	
		if (action.equalsIgnoreCase("viewAccount")){
			
			
			System.out.println(email+"Email ada ke tidak?");
			cust = CustomerDAO.getUserByEmail(email);
			
			request.setAttribute("name", cust.getCust_name());
			request.setAttribute("address", cust.getCust_address());
			request.setAttribute("email", cust.getCust_email());
			request.setAttribute("phone", cust.getCust_phoneno());
			request.setAttribute("password", cust.getCust_password());
			forward = VIEW_ACCOUNT;
			RequestDispatcher view = request.getRequestDispatcher(forward);
			view.forward(request, response);
		}
		else if (action.equalsIgnoreCase("customerAccount")){
			
			String customer = request.getParameter("user");
			
			System.out.println(email+"Email ada ke tidak?");
			cust = CustomerDAO.getUserByEmail(customer);
			
			request.setAttribute("name", cust.getCust_name());
			request.setAttribute("address", cust.getCust_address());
			request.setAttribute("email", cust.getCust_email());
			request.setAttribute("phone", cust.getCust_phoneno());
			request.setAttribute("password", cust.getCust_password());
			forward = VIEW_CUSTOMERACCOUNT;
			RequestDispatcher view = request.getRequestDispatcher(forward);
			view.forward(request, response);
		}
		else if (action.equalsIgnoreCase("updateAccount"))
		{
			cust = CustomerDAO.getUserByEmail(email);
			
			 forward = UPDATE_ACCOUNT;
			 request.setAttribute("customer", cust);
			 request.setAttribute("email", email);
			 request.setAttribute("name", cust.getCust_name());
				request.setAttribute("address", cust.getCust_address());
				request.setAttribute("email", cust.getCust_email());
				request.setAttribute("phone", cust.getCust_phoneno());
				request.setAttribute("password", cust.getCust_password());
		}

		else {
			forward = MAIN_PAGE;
		}
		RequestDispatcher view = request.getRequestDispatcher(forward);
	    view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//call logged in session
		HttpSession session = request.getSession();
		String email =(String) session.getAttribute("currentSessionUser");
				
		//call java class
		Customer cust = new Customer();
				
		//set new information
		cust.setCust_name(request.getParameter("name"));
		cust.setCust_email(email);
		cust.setCust_address(request.getParameter("address"));
		cust.setCust_phoneno(request.getParameter("phone"));
		cust.setCust_password(request.getParameter("password"));
				
			
			//call update function
			dao.updateAccount(cust);
			response.sendRedirect("AccountController?action=viewAccount");
	}

}
