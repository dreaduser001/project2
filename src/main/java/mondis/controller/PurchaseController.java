package mondis.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import javax.swing.text.Document;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import mondis.dao.PurchaseDAO;
import mondis.model.Purchase;

/**
 * Servlet implementation class PurchaseController
 */
@WebServlet("/PurchaseController")
public class PurchaseController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static String MAIN_PAGE = "adminIndex.html";
	private static String LIST_PURCHASE = "customerviewpurchases.jsp";
	private static String EACH_PURCHASE = "customervieweachpurchase.jsp";
	private static String CUSTOMEREACH_PURCHASE = "viewcustomereachpurchase.jsp";
	private static String INVOICE = "customerinvoice.jsp";
	private static String CUSTOMERINVOICE = "invoice.jsp";
	private static String LIST_ALLPURCHASE = "adminCustomerpurchases.jsp";
	private static String UPDATE_EACH_PURCHASE = "updateVieweachpurchase.jsp";
	private static String ADMIN_EACH_PURCHASE = "adminVieweachpurchase.jsp";
	private static String ADMIN_UPDATE_EACH_PURCHASE = "adminUpdateVieweachpurchase.jsp";
	ArrayList<Purchase> purchases= new ArrayList<Purchase>();
	
	private PurchaseDAO daoPurchase;
	
	String forward="";
    /**
     * Default constructor. 
     */
    public PurchaseController() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		HttpSession session = request.getSession();
		String email =(String) session.getAttribute("currentSessionUser");
		
		//CUSTOMER VIEW
		if (action.equalsIgnoreCase("listCustomerPurchase")) 
		{
			forward = LIST_PURCHASE;
            request.setAttribute("list", daoPurchase.viewPurchaseByCust(email));	
         
		}
		else if (action.equalsIgnoreCase("listofCustomersPurchase")) 
		{
			forward = LIST_ALLPURCHASE;
            request.setAttribute("list", daoPurchase.viewAllCust());	
		}
		
		else if (action.equalsIgnoreCase("viewpurchaseforcustomer")) //view customer order for customer
		{
				Purchase cust = new Purchase();
				int purchase_id = Integer.parseInt(request.getParameter("purchaseid"));
				System.out.print(purchase_id);
				cust =  daoPurchase.getViewPurchase(purchase_id);
				request.setAttribute("email", cust.getCust_email());
				request.setAttribute("name", cust.getCust_name());
				request.setAttribute("address", cust.getAddress());
				request.setAttribute("purchaseid", cust.getPurchaseid());
				request.setAttribute("date", cust.getDate());
				request.setAttribute("total", cust.getTotalamount());
	            request.setAttribute("view", daoPurchase.viewlistofeachpurchase(purchase_id));	
				forward = EACH_PURCHASE;
				

		}
		else if (action.equalsIgnoreCase("invoice")) //view customer invoice for customer
		{
				Purchase cust = new Purchase();
				int purchase_id = Integer.parseInt(request.getParameter("purchaseid"));
				System.out.print(purchase_id);
				cust =  daoPurchase.getViewPurchase(purchase_id);
				request.setAttribute("email", cust.getCust_email());
				request.setAttribute("name", cust.getCust_name());
				request.setAttribute("address", cust.getAddress());
				request.setAttribute("purchaseid", cust.getPurchaseid());
				request.setAttribute("date", cust.getDate());
				request.setAttribute("total", cust.getTotalamount());
	            request.setAttribute("view", daoPurchase.viewlistofeachpurchase(purchase_id));	
				forward = INVOICE;
				

			}
			else if (action.equalsIgnoreCase("customerorder")) //view customer order for admin
			{
				Purchase cust = new Purchase();
				int purchase_id = Integer.parseInt(request.getParameter("purchaseid"));
				System.out.print(purchase_id);
				cust =  daoPurchase.getViewPurchase(purchase_id);
				request.setAttribute("email", cust.getCust_email());
				request.setAttribute("name", cust.getCust_name());
				request.setAttribute("address", cust.getAddress());
				request.setAttribute("purchaseid", cust.getPurchaseid());
				request.setAttribute("date", cust.getDate());
				request.setAttribute("total", cust.getTotalamount());
	            request.setAttribute("view", daoPurchase.viewlistofeachpurchase(purchase_id));	
				forward = CUSTOMEREACH_PURCHASE;
				

			}			
			else if (action.equalsIgnoreCase("adminCustomer")) //view customer order for admin
			{
				Purchase cust = new Purchase();
				int purchase_id = Integer.parseInt(request.getParameter("purchaseid"));
				System.out.print(purchase_id);
				cust =  daoPurchase.getViewPurchase(purchase_id);
				request.setAttribute("email", cust.getCust_email());
				request.setAttribute("name", cust.getCust_name());
				request.setAttribute("address", cust.getAddress());
				request.setAttribute("purchaseid", cust.getPurchaseid());
				request.setAttribute("date", cust.getDate());
				request.setAttribute("total", cust.getTotalamount());
	            request.setAttribute("view", daoPurchase.viewlistofeachpurchase(purchase_id));	
				forward = ADMIN_EACH_PURCHASE;
			}
		
			else if (action.equalsIgnoreCase("adminUpdateViewEachOrder"))
			{
				Purchase cust = new Purchase();
				int purchase_id = Integer.parseInt(request.getParameter("purchaseid"));
				System.out.print(purchase_id);
				cust =  daoPurchase.getViewPurchase(purchase_id);
				request.setAttribute("email", cust.getCust_email());
				request.setAttribute("name", cust.getCust_name());
				request.setAttribute("address", cust.getAddress());
				request.setAttribute("purchaseid", cust.getPurchaseid());
				request.setAttribute("date", cust.getDate());
				request.setAttribute("total", cust.getTotalamount());
	            request.setAttribute("view", daoPurchase.viewlistofeachpurchase(purchase_id));	
				forward = ADMIN_UPDATE_EACH_PURCHASE;
			}
		
			else 
		{
			forward = MAIN_PAGE;
		}
		
		RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		HttpSession session = request.getSession();
		String email =(String) session.getAttribute("currentSessionUser");
		
		//DELETE FOR ADMIN
		if (action.equalsIgnoreCase("Delete")) 
		{
			int purchase_id = Integer.parseInt(request.getParameter("purchaseid"));
			System.out.print(purchase_id);
			daoPurchase.deletePurchase(purchase_id);
			
			forward = LIST_PURCHASE;
			response.sendRedirect("PurchaseController?action=ListofCustomersPurchase");
		}
		
		else if (action.equalsIgnoreCase("Submit")) 
		{
		
			
			int purchase_id = Integer.parseInt(request.getParameter("purchaseid"));
			String date = request.getParameter("date");
			String stritemindex = request.getParameter("itemindex");
			String address = request.getParameter("address");
			String produk[] = request.getParameterValues("product_id");
			String jumlah[] = request.getParameterValues("quantity");
			String harga[] = request.getParameterValues("price");
			
			for(int i = 0 ; i<produk.length; i++)
			{
				Purchase products = new Purchase();
				
				products.setproduct_id(produk[i]);
				products.setStringQuantity(jumlah[i]);
				products.setUnitPrice(Float.parseFloat(harga[i]));
				System.out.print("ProductID: "+produk[i]+"\nQuantity: " + jumlah[i] + "\nHarga: "+ harga[i]+"\nprice");
				purchases.add(products);
				System.out.print(purchases);	
								
				
			}
			daoPurchase.updatePurchase(purchase_id, date, address, purchases);
			
			
			forward = ADMIN_UPDATE_EACH_PURCHASE;
			response.sendRedirect("PurchaseController?action=adminCustomer&purchaseid="+purchase_id);
		}
		else if (action.equalsIgnoreCase("Generate")) 
		{
			int purchase_id = Integer.parseInt(request.getParameter("purchaseid"));
			Purchase cust = new Purchase();
			cust =  daoPurchase.getViewPurchase(purchase_id);
			  try {

		        } catch (Exception e) {
		        }
		
		}
	}
}
