package mondis.controller;

import java.io.IOException;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Cookie;
import mondis.dao.AdminDAO;
import mondis.model.Admin;
import mondis.dao.CustomerDAO;
import mondis.model.Customer;

/**
 * Servlet implementation class LoginController
 */
@WebServlet("/LoginController")
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
	public LoginController() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try { 
			Admin admin = new Admin(); 
			admin.setAdmin_email(request.getParameter("admin_email")); 
			admin.setAdmin_password(request.getParameter("admin_password")); 
			
			admin = AdminDAO.login(admin); 
			
			if (admin.isValid()) { 
				HttpSession session = request.getSession(true); 
				session.setAttribute("currentSessionAdmin", admin.getAdmin_email()); 
				response.sendRedirect("adminIndex.jsp"); // logged-in page 
				} 
			
			else 
				response.sendRedirect("adminInvalidLogin.jsp"); // error page 
			} 
		
		catch (Throwable theException) { 
			System.out.println(theException); 
			}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try
		{       

		     Customer cust = new Customer();
		     cust.setCust_email(request.getParameter("email"));
		     cust.setCust_password(request.getParameter("password"));
		 
		   

		     cust = CustomerDAO.login(cust);
		                  
		     if (cust.isValid())
		     {
		         
		    	 	HttpSession session = request.getSession(true);
					session.setAttribute("currentSessionUser", cust.getCust_email()); //set current session based on email
					//response.sendRedirect("/custExample/home.jsp"); // logged-in page
					request.setAttribute("cust", CustomerDAO.getUserByEmail(cust.getCust_email()));   //to retrieve user info 
					
					Cookie ck=new Cookie("email",cust.getCust_email());//creating cookie object  
				    response.addCookie(ck);
				    
					RequestDispatcher view = request.getRequestDispatcher("customerindex.jsp"); // logged-in page
					view.forward(request, response);  
					
		     }
		     else 
					response.sendRedirect("invalidLogin.jsp"); // error page 
		}
		       
		       
		catch (Throwable theException)        
		{
		     System.out.println(theException);
		}
  }
		

}
