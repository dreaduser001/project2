package mondis.controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import mondis.dao.CustomerDAO;
import mondis.model.Customer;

/**
 * Servlet implementation class RegisterController
 */
@WebServlet("/RegisterController")
public class RegisterController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private CustomerDAO dao; 
    /**
     * Default constructor. 
     */
    public RegisterController() {
    	super();
    	dao = new CustomerDAO();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Customer cust = new Customer();
		//retrieve input and set
		cust.setCust_name(request.getParameter("name"));
		cust.setCust_email(request.getParameter("email"));
		cust.setCust_address(request.getParameter("address"));
		cust.setCust_icno(request.getParameter("ic_no"));
		cust.setCust_phoneno(request.getParameter("phoneno"));
		cust.setCust_password(request.getParameter("password"));
		
		cust = CustomerDAO.getCustomer(cust);
		//check if cust exists
		if(!cust.isValid()){
        		System.out.println("inserting customer");
				try {
					dao.add(cust);
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				}
			}
        	//redirect to login page
        	response.sendRedirect("customerlogin.jsp");
        }
}
