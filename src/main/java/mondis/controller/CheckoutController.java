package mondis.controller;

import java.io.IOException;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import mondis.dao.CustomerDAO;
import mondis.model.Customer;
import mondis.dao.CartDAO;

/**
 * Servlet implementation class CheckoutController
 */
@WebServlet("/CheckoutController")
public class CheckoutController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static String VIEW_CHECKOUT = "checkout.jsp";
	private static String MAIN_PAGE = "customerindex.jsp";
    /**
     * Default constructor. 
     */
    public CheckoutController() {
        // TODO Auto-generated constructor stub
    }

    String forward;
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Customer cust = new Customer();
		String action = request.getParameter("action");
		HttpSession session = request.getSession();
		String email =(String) session.getAttribute("currentSessionUser");
		
	
		if (action.equalsIgnoreCase("view")){
			
			
			System.out.println(email+"Email ada ke tidak?");
			cust = CustomerDAO.getUserByEmail(email);
			
			request.setAttribute("name", cust.getCust_name());
			request.setAttribute("email", cust.getCust_email());
			forward = VIEW_CHECKOUT;
			RequestDispatcher view = request.getRequestDispatcher(forward);
			view.forward(request, response);
		}

		else {
			forward = MAIN_PAGE;
		}
		RequestDispatcher view = request.getRequestDispatcher(forward);
	    view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String email =(String) session.getAttribute("currentSessionUser");
		
		//call java class
		String strItemIndex = request.getParameter("itemIndex");
		String name = request.getParameter("name");
		String date = request.getParameter("date");
		String address = request.getParameter("address");
		String product_id = request.getParameter("product_id");
		int quantity = Integer.parseInt(request.getParameter("quantity"));
		
		
		System.out.print(name + date +address +product_id+quantity);
		Double ta = Double.parseDouble(request.getParameter("purchaseTotal"));
		System.out.print(ta);
		//set new information
		
		
		
		CartDAO dao = new CartDAO();
		dao.add(email, date, address, product_id, quantity, ta, strItemIndex);
		response.sendRedirect("PurchaseController?action=listCustomerPurchase");
	}

}
