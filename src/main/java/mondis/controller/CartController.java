package mondis.controller;

import java.io.IOException;
import java.util.ArrayList;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import mondis.dao.CartDAO;
import mondis.model.Purchase;

/**
 * Servlet implementation class CartController
 */
@WebServlet("/CartController")
public class CartController extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public CartController() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String strAction = request.getParameter("action");

		if(strAction!=null && !strAction.equals("")) {
			if(strAction.equalsIgnoreCase("add")) {
				addToCart(request);
			} else if (strAction.equalsIgnoreCase("Update")) {
				updateCart(request);
			} else if (strAction.equalsIgnoreCase("Delete")) {
				deleteCart(request);
			}
				
		response.sendRedirect("cart.jsp");
			}
	}
	
	protected void deleteCart(HttpServletRequest request) {
		HttpSession session = request.getSession();
		String itemIndex = request.getParameter("itemIndex");
		CartDAO cartBean = null;

		Object objCartBean = session.getAttribute("cart");
		if(objCartBean!=null) {
			cartBean = (CartDAO) objCartBean ;
		} else {
			cartBean = new CartDAO();
		}
		cartBean.deleteCartItem(itemIndex);
	}
	
	protected void updateCart(HttpServletRequest request) {
		HttpSession session = request.getSession();
		String strItemIndex = request.getParameter("itemIndex");
		String quantity = request.getParameter("quantity");
		
		CartDAO cartBean = null;

		Object objCartBean = session.getAttribute("cart");
		if(objCartBean!=null) {
			cartBean = (CartDAO) objCartBean ;
		} else {
			cartBean = new CartDAO();
		}
		cartBean.updateCartItem(strItemIndex, quantity);
	}

	protected void addToCart(HttpServletRequest request) {
		ArrayList<Purchase> cartItems = new ArrayList<Purchase>();
		HttpSession session = request.getSession();
	    
		String id = request.getParameter("product_id");
		String name = request.getParameter("product_name");
		String price = request.getParameter("product_unitprice");
		String quantity = request.getParameter("quantity");

		System.out.println(id+ name+price+quantity);
		
		CartDAO cartBean = null;

		Object objCartBean = session.getAttribute("cart");

		if(objCartBean!=null) {
			cartBean = (CartDAO) objCartBean ;
		} else {
			cartBean = new CartDAO();
			session.setAttribute("cart", cartBean);
		}

		cartBean.addCartItem(id, name, price, quantity);
	}

}
