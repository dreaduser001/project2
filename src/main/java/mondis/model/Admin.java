package mondis.model;

public class Admin {
	
	private String admin_name;
	private String admin_address;
	private String admin_phone;
	private String admin_email;
	private String admin_ic;
	private String admin_password;
	public boolean valid;
	
	//Default constructor
	public Admin() {
	}

	public Admin(String admin_name, String admin_address, String admin_phone, String admin_email,
			String admin_ic, String admin_password) {
		
		this.admin_name = admin_name;
		this.admin_address = admin_address;
		this.admin_phone = admin_phone;
		this.admin_email = admin_email;
		this.admin_ic = admin_ic;
		this.admin_password = admin_password;
	}
	

	public String getAdmin_name() {
		return admin_name;
	}

	public void setAdmin_name(String admin_name) {
		this.admin_name = admin_name;
	}

	public String getAdmin_address() {
		return admin_address;
	}

	public void setAdmin_address(String admin_address) {
		this.admin_address = admin_address;
	}

	public String getAdmin_phone() {
		return admin_phone;
	}

	public void setAdmin_phone(String admin_phone) {
		this.admin_phone = admin_phone;
	}

	public String getAdmin_email() {
		return admin_email;
	}

	public void setAdmin_email(String admin_email) {
		this.admin_email = admin_email;
	}

	public String getAdmin_ic() {
		return admin_ic;
	}

	public void setAdmin_ic(String admin_ic) {
		this.admin_ic = admin_ic;
	}

	public String getAdmin_password() {
		return admin_password;
	}

	public void setAdmin_password(String admin_password) {
		this.admin_password = admin_password;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}
	
}
