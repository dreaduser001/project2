package mondis.model;

import java.util.ArrayList;


public class Cart {

	private ArrayList<Purchase> cartItems;
    private int purchaseid;
    public String product_id;
    public String staff_id;
    public String cust_email;
	public String product_name;
	public String date;
	public String address;
	public int orderQtty;
	public double unitPrice;
	public double totalamount;
	public boolean valid;
	
	public Cart() {
		super();
	}

	public Cart(int purchaseid, String product_id, String staff_id, String cust_email, String product_name, String date,
			String address, int orderQtty, double unitPrice, double totalamount, boolean valid) {
		super();
		this.purchaseid = purchaseid;
		this.product_id = product_id;
		this.staff_id = staff_id;
		this.cust_email = cust_email;
		this.product_name = product_name;
		this.date = date;
		this.address = address;
		this.orderQtty = orderQtty;
		this.unitPrice = unitPrice;
		this.totalamount = totalamount;
		this.valid = valid;
	}

	public int getPurchaseid() {
		return purchaseid;
	}

	public void setPurchaseid(int purchaseid) {
		this.purchaseid = purchaseid;
	}

	public String getProduct_id() {
		return product_id;
	}

	public void setProduct_id(String product_id) {
		this.product_id = product_id;
	}

	public String getStaff_id() {
		return staff_id;
	}

	public void setStaff_id(String staff_id) {
		this.staff_id = staff_id;
	}

	public String getCust_email() {
		return cust_email;
	}

	public void setCust_email(String cust_email) {
		this.cust_email = cust_email;
	}

	public String getProduct_name() {
		return product_name;
	}

	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getOrderQtty() {
		return orderQtty;
	}

	public void setOrderQtty(int orderQtty) {
		this.orderQtty = orderQtty;
	}

	public double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public double getTotalamount() {
		return totalamount;
	}

	public void setTotalamount(double totalamount) {
		this.totalamount = totalamount;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}
	
	public ArrayList<Purchase> getCartItems() {
		return cartItems;
	}
}
