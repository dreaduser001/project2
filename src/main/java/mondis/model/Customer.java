package mondis.model;

public class Customer {

	private String name;
	private String email;
	private String ic_no;
	private String phoneno;
	private String address;
	private String password;
	public boolean valid;
	

	public Customer() {
		super();
	}

	public String getCust_name() {
		return name;
	}
	public String getCust_email() {
		return email;
	}
	public String getCust_icno() {
		return ic_no;
	}
	public String getCust_phoneno() {
		return phoneno;
	}
	public String getCust_address() {
		return address;
	}
	public String getCust_password() {
		return password;
	}
	public boolean isValid() {
		return valid;
	}
	
	public void setCust_name(String name) {
		this.name = name;
	}
	public void setCust_email(String email) {
		this.email = email;
	}
	public void setCust_icno(String ic_no) {
		this.ic_no = ic_no;
	}
	public void setCust_phoneno(String phoneno) {
		this.phoneno = phoneno;
	}
	public void setCust_address(String address) {
		this.address = address;
	}
	public void setCust_password(String password) {
		this.password = password;
	}
	public void setValid(boolean valid) {
		this.valid = valid;
	}
}
