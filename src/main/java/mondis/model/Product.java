package mondis.model;

public class Product {
	
	private String product_id;
	private String product_name;
	private double product_unitprice;
	private String product_type;
	private String email;
	public boolean valid;
	
	public Product() {
		super();
	}

	public Product(String product_id, String product_name, String product_type, String email,double product_unitprice
			) {
		super();
		this.product_id = product_id;
		this.product_name = product_name;
		this.product_unitprice = product_unitprice;
		this.product_type = product_type;
		this.email = email;
	}

	public String getProduct_id() {
		return product_id;
	}

	public void setProduct_id(String product_id) {
		this.product_id = product_id;
	}

	public String getProduct_name() {
		return product_name;
	}

	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}

	public double getProduct_unitprice() {
		return product_unitprice;
	}

	public void setProduct_unitprice(double product_unitprice) {
		this.product_unitprice = product_unitprice;
	}

	public String getProduct_type() {
		return product_type;
	}

	public void setProduct_type(String product_type) {
		this.product_type = product_type;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}
	
	
}
