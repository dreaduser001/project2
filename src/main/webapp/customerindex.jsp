<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Customer Homepage</title>
<link rel="stylesheet" href="style.css">
</head>
<body>
<div class="topnav">
  <a class="active" href="customerindex.jsp">Home</a>
  <a href="ProductController?action=listCustomerProduct">Product</a>
  <a href="ProductController?action=listOrderProduct">Order Now</a>
  <div class="topnav-right">
    <div class="dropdown">
    <button class="dropbtn">My Account
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="AccountController?action=viewAccount"><i class="fa fa-user"></i>Profile</a>
      <a href="PurchaseController?action=listCustomerPurchase"><i class="fa fa-shopping-cart"></i>Order</a>
      <a href="CheckoutController?action=view"><i class="fa fa-crosshairs"></i> Checkout</a>
      <a href="cart.jsp"  ><i class="fa fa-shopping-cart" ></i> Cart</a>
    </div>
  </div>
    <a href="LogoutController"><i class="fa fa-lock"></i> Logout</a>
  </div>
</div>

<h1><span>Mondis SDN BHD</h1>
<h2></h2>
<p>We provide the best company services to you. Our company is founded in 2006 with best knowledge to provide services that may suits your prefererences.
<br>Order Now.
</p>
</body>
</html>