<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Update Customer Account</title>
<link rel="stylesheet" href="style.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>
<div class="topnav">
  <a class="active" href="customerindex.jsp">Home</a>
  <a href="ProductController?action=listCustomerProduct">Product</a>
  <a href="ProductController?action=listOrderProduct">Order Now</a>
  <div class="topnav-right">
    <div class="dropdown">
    <button class="dropbtn">My Account
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="AccountController?action=viewAccount"><i class="fa fa-user"></i>Profile</a>
      <a href="OrderController?action=listCustomerOrder"><i class="fa fa-shopping-cart"></i>Order</a>
      <a href="CheckoutController?action=view"><i class="fa fa-crosshairs"></i> Checkout</a>
      <a href="cart.jsp"  ><i class="fa fa-shopping-cart" ></i> Cart</a>
    </div>
  </div>
    <a href="LogoutController"><i class="fa fa-lock"></i> Logout</a>
  </div>
</div>

<h1>Customer Account</h1>
<form method="post" action="AccountController">
	<div class="container">
	<label for="username"><b>Username</b></label>
    <input type="text" name="name" id="name" placeholder="New Name" value="<%= request.getAttribute("name") %>">
	<label for="password"><b>Password</b></label>
	<input type="password" name="password" id="Mypassword" placeholder="New Password" value="<%= request.getAttribute("password") %>">
	<label for="email"><b>Email</b></label>
	<input type="email" name="email" id="email" placeholder="<%= request.getAttribute("email") %>" readonly value="<%= request.getAttribute("email") %>">  
	<label for="address"><b>Home Address</b></label>
	<input type="text" name="address" id="address" placeholder="New Address" value="<%= request.getAttribute("address") %>"> 
	<label for="phoneno"><b>Phone No.</b></label>
	<input type="tel" name="phone" id="phone" pattern="[0-9]{10}" placeholder="New Phone no" value="<%= request.getAttribute("phone") %>"> 
	<span>
		<input type="checkbox" onclick="myFunction()">Show Password
	</span>
	<br><br>
	<button class="btn btn-primary pull-left">Confirm</button>
	</div>
</form>

<script>
	function myFunction() {
		  var x = document.getElementById("Mypassword");
		  if (x.type === "password") {
		    x.type = "text";
		  } else {
		    x.type = "password";
		  }
		}
</script>
</body>
</html>