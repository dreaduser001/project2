<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Contact Us page</title>
<link rel="stylesheet" href="style.css">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
body {font-family: "Lato", sans-serif}
.mySlides {display: none}
</style>
</head>
<body>
<div class="topnav">
  <a class="active" href="index.jsp">Home</a>
  <a href="ProductController?action=listProduct">Product</a>
  <a href="contact.jsp">Contact</a>
  <div class="topnav-right">
    <div class="dropdown">
    <button class="dropbtn">Login
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="adminLogin.jsp">Admin Login</a>
      <a href="customerlogin.jsp">Customer Login</a>
    </div>
  </div>
    <a href="register.jsp">Register</a>
  </div>
</div>

<!-- Page content -->
<div class="w3-content" style="max-width:2000px;margin-top:46px">


  <!-- The Contact Section -->
  <div class="w3-container w3-content w3-padding-64" style="max-width:800px" id="contact">
    <h2 class="w3-wide w3-center">CONTACT US</h2>
    <div class="w3-row w3-padding-32">
      <div class="w3-col m6 w3-large w3-margin-bottom">
        <i class="fa fa-map-marker" style="width:30px"></i> Jasin, Melaka<br>
        <i class="fa fa-phone" style="width:30px"></i> Phone: +03 453628<br>
        <i class="fa fa-envelope" style="width:30px"> </i> Email: jasinesport@uitm.edu.my<br>
        <i class="" style="width:30px"> </i> Link: https://www.uitm.edu.my/index.php/en/<br>
      </div>
    </div>
  </div>
  
<!-- End Page Content -->
</div>

</body>
</html>