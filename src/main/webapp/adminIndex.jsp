<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin Home Page</title>
<link rel="stylesheet" href="style.css">
</head>
<body>

<div class="topnav">
  <a class="active" href="adminIndex.jsp">Home</a>
  <a href="ProductController?action=listAdminProduct">Product</a>
  <a href="PurchaseController?action=listofCustomersPurchase">Order list</a>
  <div class="topnav-right">
    <a href="LogoutController"><i class="fa fa-lock"></i> Logout</a>
  </div>
</div>

<h1>Admin Home page</h1>
</body>
</html>