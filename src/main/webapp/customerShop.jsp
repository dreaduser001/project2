<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Customer Shop Page</title>
<link rel="stylesheet" href="style.css">
</head>
<body>
<div class="topnav">
  <a class="active" href="customerindex.jsp">Home</a>
  <a href="ProductController?action=listCustomerProduct">Product</a>
  <a href="ProductController?action=listOrderProduct">Order Now</a>
  <div class="topnav-right">
    <div class="dropdown">
    <button class="dropbtn">My Account
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="AccountController?action=viewAccount"><i class="fa fa-user"></i>Profile</a>
      <a href="OrderController?action=listCustomerOrder"><i class="fa fa-shopping-cart"></i>Order</a>
      <a href="CheckoutController?action=view"><i class="fa fa-crosshairs"></i> Checkout</a>
      <a href="cart.jsp"  ><i class="fa fa-shopping-cart" ></i> Cart</a>
    </div>
  </div>
    <a href="LogoutController"><i class="fa fa-lock"></i> Logout</a>
  </div>
</div>

<h1>Product Available Now</h1>
<p>Below is the information about the product that are available.</p>
<p>If you want to purchase the product please click the Order Now in the navbar above.</p>

<b>1)Subscription</b>

<table border=1>
	<thead>
		<tr>
			<td>Product Name
			<td>Product Type
			<td>Product Price
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${subscription}" var="product">
			<tr>
				<td>${product.product_name }</td>
				<td>${product.product_type }</td>
				<td>RM ${product.product_unitprice }</td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<b>2)Channel</b>

<table border=1>
	<thead>
		<tr>
			<td>Product Name
			<td>Product Type
			<td>Product Price
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${channel}" var="product">
			<tr>
				<td>${product.product_name }</td>
				<td>${product.product_type }</td>
				<td>RM ${product.product_unitprice }</td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<b>3)Services</b>

<table border=1>
	<thead>
		<tr>
			<td>Product Name
			<td>Product Type
			<td>Product Price
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${services}" var="product">
			<tr>
				<td>${product.product_name }</td>
				<td>${product.product_type }</td>
				<td>RM ${product.product_unitprice }</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
</body>
</html>