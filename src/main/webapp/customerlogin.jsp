<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Customer Login Page</title>
<link rel="stylesheet" href="style.css">
</head>
<body>
<div class="topnav">
  <a class="active" href="index.jsp">Home</a>
  <a href="ProductController?action=listProduct">Product</a>
  <a href="#contact">Contact</a>
  <div class="topnav-right">
    <div class="dropdown">
    <button class="dropbtn">Login
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="adminLogin.jsp">Admin Login</a>
      <a href="customerlogin.jsp">Customer Login</a>
    </div>
  </div>
    <a href="register.jsp">Register</a>
  </div>
</div>

<h2>Login to your account</h2>
	<form method="post" action="LoginController">
		<input type="email" name="email" id="email" placeholder="Email Address" />	
		<input type="password" name="password" id="Mypassword" placeholder="Password" />
		<span>
			<input type="checkbox" onclick="myFunction()">Show Password
		</span>	
		<br>
	<button class="btn btn-default linklog" type="submit" name ="button-login">Login</button>
	</form>
<script>
	function myFunction() {
		  var x = document.getElementById("Mypassword");
		  if (x.type === "password") {
		    x.type = "text";
		  } else {
		    x.type = "password";
		  }
		}
</script>
</body>
</html>