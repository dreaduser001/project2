<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin Login Page</title>
<link rel="stylesheet" href="style.css">
</head>
<body>
<div class="topnav">
  <a class="active" href="index.jsp">Home</a>
  <a href="ProductController?action=listProduct">Product</a>
  <a href="#contact">Contact</a>
  <div class="topnav-right">
    <div class="dropdown">
    <button class="dropbtn">Login
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="adminLogin.jsp">Admin Login</a>
      <a href="customerlogin.jsp">Customer Login</a>
    </div>
  </div>
    <a href="register.jsp">Register</a>
  </div>
</div>

<h2>Login to Admin account</h2>
	<form action="LoginController">
		<input type="email" placeholder="Admin Email" name="admin_email"/>
		<input type="password" placeholder="Admin Password" name="admin_password"/>
	<span>
		<input type="checkbox" class="checkbox"> 
		Keep me signed in
	</span>
		<button type="submit" class="btn btn-default">Login</button>
	</form>
</body>
</html>