<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Account Customer</title>
<link rel="stylesheet" href="style.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>
<div class="topnav">
  <a class="active" href="customerindex.jsp">Home</a>
  <a href="ProductController?action=listCustomerProduct">Product</a>
  <a href="ProductController?action=listOrderProduct">Order Now</a>
  <div class="topnav-right">
    <div class="dropdown">
    <button class="dropbtn">My Account
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="AccountController?action=viewAccount"><i class="fa fa-user"></i>Profile</a>
      <a href="OrderController?action=listCustomerOrder"><i class="fa fa-shopping-cart"></i>Order</a>
      <a href="CheckoutController?action=view"><i class="fa fa-crosshairs"></i> Checkout</a>
      <a href="cart.jsp"  ><i class="fa fa-shopping-cart" ></i> Cart</a>
    </div>
  </div>
    <a href="LogoutController"><i class="fa fa-lock"></i> Logout</a>
  </div>
</div>

<h1>Customer Account</h1>
<form>
	<div class="container">
	<label for="username"><b>Username</b></label>
    <input type="text" value="<%= request.getAttribute("name")%>"/>
	<label for="password"><b>Password</b></label>
	<input type="password" readonly type="password" value="<%= request.getAttribute("password") %>"/>
	<label for="email"><b>Email</b></label>
	<input type="email" readonly value="<%= request.getAttribute("email")%>"/>
	<label for="address"><b>Home Address</b></label>
	<input type="text" readonly value="<%= request.getAttribute("address")%>"/> 
	<label for="phoneno"><b>Phone No.</b></label>
	<input type="tel" readonly value="<%= request.getAttribute("phone") %>"/>
	
	<a class="btn btn-primary pull-left" href="AccountController?action=updateAccount">Update</a>
	</div>
</table>	
</form>
</body>
</html>