<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Invalid Admin Login Page</title>
<link rel="stylesheet" href="style.css">
</head>
<body>
<div class="topnav">
  <a class="active" href="index.jsp">Home</a>
  <a href="ProductController?action=listProduct">Product</a>
  <a href="#contact">Contact</a>
  <div class="topnav-right">
    <div class="dropdown">
    <button class="dropbtn">Login
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="adminLogin.jsp">Admin Login</a>
      <a href="customerlogin.jsp">Customer Login</a>
    </div>
  </div>
    <a href="register.jsp">Register</a>
  </div>
</div>

<h1>Invalid Admin Login!</h1>
<p>Sorry, you are not Admin! Please login in customer section</p>
</body>
</html>