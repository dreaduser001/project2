<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Update Product</title>
<link rel="stylesheet" href="style.css">
</head>
<body>

<div class="topnav">
  <a class="active" href="adminIndex.jsp">Home</a>
  <a href="ProductController?action=listAdminProduct">Product</a>
  <a href="#contact">Order list</a>
  <div class="topnav-right">
    <a href="LogoutController"><i class="fa fa-lock"></i> Logout</a>
  </div>
</div>

<h1>Update Product</h1>
<form id="upProductForm" name="upProductForm" method="post" action="ProductController">
	<label for="product_id">Product ID:</label><br>
	<input type="text" id="product_id" name="product_id" value="${product.product_id}" readonly><br>
							
	<label for="product_name">Product Name:</label><br>
	<input type="text" id="product_name" name="product_name" value="${product.product_name}"><br>
							
	<label for="product_unitPrice">Product Price:</label><br>
	<input type="number" id="product_unitPrice" name="product_unitPrice" value="${product.product_unitPrice}"><br><br>
							
	<label for="product_type">Product Type:</label><br>
	<input type="text" id="product_type" name="product_type" value="${product.product_type}"><br>
							
	<input type="submit" class="btn btn-default linklog" value="Submit">
</form>
<a class="btn btn-primary" href="ProductController?action=listAdminProduct">Back</a>
</body>
</html>