<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin view All Customer Order</title>
<link rel="stylesheet" href="style.css">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/prettyPhoto.css" rel="stylesheet">
<link href="css/price-range.css" rel="stylesheet">
<link href="css/animate.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">
<link href="css/loginheader.css" rel="stylesheet">
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
</head>
<body>

<div class="topnav">
  <a class="active" href="adminIndex.jsp">Home</a>
  <a href="ProductController?action=listAdminProduct">Product</a>
  <a href="PurchaseController?action=listofCustomersPurchase">Order list</a>
  <div class="topnav-right">
    <a href="LogoutController"><i class="fa fa-lock"></i> Logout</a>
  </div>
</div>

<h4>View All Your Orders</h4>
<div class="table-responsive cart_info">
<table class="table table-condensed">
	<thead>
	<tr class="cart_menu">
		<td class="image">Order ID</td>
		<td class="description" >Date</td>
		<td class="quantity">View</td>
		<td class="quantity">Invoice</td>
		<td class="quantity"> Edit</td>
		<td class="quantity"> Delete</td>
     </tr>
	</thead>
	<tbody>
	<c:forEach items="${list}" var="purchase">
	<tr>
		<td class="cart_product">
		<h4><c:out value="${purchase.orderid}"/></a><h4>
		</td>
		<td class="cart_description">
			<h4><c:out value="${purchase.date}" /></h4>
		</td>
		<td class="cart_quantity">
		<a class="btn btn-primary" href="PurchaseController?action=adminCustomer&purchaseid=${purchase.purchaseid}">View</a>
		</td>	
		<td class="cart_quantity">
			<form method="POST" action="PurchaseController">
				<input type="hidden" id="purchaseid" name="purchaseid" value="${purchase.purchaseid}"/>
				<input onclick="return confirm('Are you sure to generate the bill into pdf file?')"type="submit" class="btn btn-primary" name="action" value="Generate"></input>
			</form>
		</td>
		<td class="cart_quantity">
			<a class="btn btn-primary" href="PurchaseController?action=adminUpdateViewEachOrder&purchaseid=${order.orderid}">Update</a>
		</td>
		<td class="cart_quantity">
			<form method="POST" action="PurchaseController">
			<input type="hidden" id="purchaseid" name="purchaseid" value="${order.orderid}">
			<input onclick="return confirm('Are you sure?')"type="submit" class="btn btn-primary" name="action" value="Delete"></input>
			</form>
		</td>
		</tr>
	</c:forEach>
</tbody>
</table>			
</div>
</body>
</html>