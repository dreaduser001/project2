<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Cart page</title>
<link rel="stylesheet" href="style.css">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/prettyPhoto.css" rel="stylesheet">
<link href="css/price-range.css" rel="stylesheet">
<link href="css/animate.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">
<link href="css/loginheader.css" rel="stylesheet">
<link href="css/image.css" rel="stylesheet">
</head>
<body>
<div class="topnav">
  <a class="active" href="customerindex.jsp">Home</a>
  <a href="ProductController?action=listCustomerProduct">Product</a>
  <a href="ProductController?action=listOrderProduct">Order Now</a>
  <div class="topnav-right">
    <div class="dropdown">
    <button class="dropbtn">My Account
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="AccountController?action=viewAccount"><i class="fa fa-user"></i>Profile</a>
      <a href="PurchaseController?action=listCustomerPurchase"><i class="fa fa-shopping-cart"></i>Order</a>
      <a href="CheckoutController?action=view"><i class="fa fa-crosshairs"></i> Checkout</a>
      <a href="cart.jsp"  ><i class="fa fa-shopping-cart" ></i> Cart</a>
    </div>
  </div>
    <a href="LogoutController"><i class="fa fa-lock"></i> Logout</a>
  </div>
</div>

<section id="cart_items">
	
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Shopping Cart</li>
				</ol>
			</div>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_product">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
					
					<jsp:useBean id="cart" scope="session" class="mondis.dao.CartDAO" />
  						<c:if test="${cart.lineItemCount==0}">
  						  <tr>
						  <td colspan="4" style="border:none">- Cart is currently empty -<br/>
						  </tr>
						  </c:if>
 					 <c:forEach var="cartItem" items="${cart.cartItems}" varStatus="counter">
 					 <form method="POST" action="CartController">
						<tr>
						
								<input type="hidden" name="itemIndex" value="${counter.count}">
								<input type="hidden" name="product_id" value="${cartItem.product_id}">
							<td class="cart_image">
								
								<input type="hidden" name="product_image" value="${cartItem.base64Image}">
								<p class="cart_total_price"><img src="data:image/jpg;base64,${cartItem.base64Image}" alt="" /></p>
							</td>
							<td class="cart_description">
								
								<input type="hidden" name="product_name" value="${cartItem.product_name} ">
								<p class="cart_total_price">${cartItem.product_name}</p>
							</td>
							<td class="cart_price">
								<input type="hidden" name="price" value="${cartItem.unitPrice}">
								<p>RM ${cartItem.unitPrice}</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
								 
									<input class="cart_quantity_input" type="text" name="quantity" placeholder="${cartItem.orderQtty}" size="2">
									
								</div>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">RM ${cartItem.totalamount}</p>
							</td>
							 <td class="cart_delete">
								<input type="submit" class="cart_quantity_delete btn btn-primary" name="action" value="Update"></input>
							<input type="submit" class="cart_quantity_delete btn btn-primary" name="action" value="Delete"></input></p>
							
							</td>
							
						</tr>
						</form>
						</c:forEach>
					</tbody>
				</table>
				
			</div>
		</div>
	</section>
</body>
</html>