<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Product List</title>
<link rel="stylesheet" href="style.css">
</head>
<body>
<div class="topnav">
  <a class="active" href="index.jsp">Home</a>
  <a href="ProductController?action=listProduct">Product</a>
  <a href="#contact">Contact</a>
  <div class="topnav-right">
    <div class="dropdown">
    <button class="dropbtn">Login
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="adminLogin.jsp">Admin Login</a>
      <a href="customerlogin.jsp">Customer Login</a>
    </div>
  </div>
    <a href="register.jsp">Register</a>
  </div>
</div>

<h1>Product List by Type</h1>

<b>1)Subscription</b>
<table border=1>
	<thead>
		<tr>
			<td>Product Name
			<td>Product Type
			<td>Product Price
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${subscription}" var="product">
			<tr>
				<td>${product.product_name }</td>
				<td>${product.product_type }</td>
				<td>RM ${product.product_unitprice }</td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<b>2)Channel</b>
<table border=1>
	<thead>
		<tr>
			<td>Product Name
			<td>Product Type
			<td>Product Price
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${channel}" var="product">
			<tr>
				<td>${product.product_name }</td>
				<td>${product.product_type }</td>
				<td>RM ${product.product_unitprice }</td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<b>3)Services</b>
<table border=1>
	<thead>
		<tr>
			<td>Product Name
			<td>Product Type
			<td>Product Price
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${services}" var="product">
			<tr>
				<td>${product.product_name }</td>
				<td>${product.product_type }</td>
				<td>RM ${product.product_unitprice }</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
</body>
</html>