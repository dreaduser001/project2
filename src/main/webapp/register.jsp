<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Register Page</title>
<link rel="stylesheet" href="style.css">
</head>
<body>
<div class="topnav">
  <a class="active" href="index.jsp">Home</a>
  <a href="ProductController?action=listProduct">Product</a>
  <a href="#contact">Contact</a>
  <div class="topnav-right">
    <div class="dropdown">
    <button class="dropbtn">Login
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="adminLogin.jsp">Admin Login</a>
      <a href="customerlogin.jsp">Customer Login</a>
    </div>
  </div>
    <a href="register.jsp">Register</a>
  </div>
</div>

<h2>New User Register!</h2>
	<form method="post" action="RegisterController">
	<div class="container">
	<label for="username"><b>Username</b></label>
    <input type="text" placeholder="Enter Username" name="name" id="name" size ="20" required>
	<label for="password"><b>Password</b></label>
	<input type="password" placeholder="Password" id="password" name="password" required>
	<label for="ic_no"><b>NRIC</b></label>
	<input type="text" placeholder="NRIC" id="ic_no" name="ic_no" required>
	<label for="email"><b>Email</b></label>
	<input type="email" placeholder="Email" id="email" name="email" required>
	<label for="address"><b>Address</b></label>
	<input type="text" id="address" name="address" placeholder="Home Address" required>
	<label for="phoneno"><b>Phone No.</b></label>
	<input type="tel" id="phoneno" name="phoneno" pattern="[0-9]{10}" placeholder="Phone Number - Example: 0115421234" required>
	
	<button type="submit" class="btn btn-default">Register</button>
	</div>
	</form>
</body>
</html>