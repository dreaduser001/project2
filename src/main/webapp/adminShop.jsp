<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin Product Shop</title>
<link rel="stylesheet" href="style.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>

<div class="topnav">
  <a class="active" href="adminIndex.jsp">Home</a>
  <a href="ProductController?action=listAdminProduct">Product</a>
  <a href="PurchaseController?action=listofCustomersPurchase">Order list</a>
  <div class="topnav-right">
    <a href="LogoutController"><i class="fa fa-lock"></i> Logout</a>
  </div>
</div>

<h1>Product List</h1>
<p></p>

<b>1)Subscription</b>
<a class="btn btn-primary" href="addSubscription.html">Add</a>
<br>
<table border=1>
	<thead>
		<tr>
			<td>Product Name
			<td>Product Price
			<td>Action  
		</tr>
	</thead>
	<tbody>
		<c:forEach var="product" items="${subscription}" >
			<tr>
				<td>${product.product_name }</td>
				<td>RM ${product.product_unitprice }</td>
				<td>
					<a href="ProductController?action=updateProduct&id=${product.product_id }">Update</a>&nbsp;
					<a href="ProductController?action=deleteProduct&id=${product.product_id }">Delete</a>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<b>2)Channel</b>
<a class="btn btn-primary" href="addChannel.html">Add</a>
<br>
<table border=1>
	<thead>
		<tr>
			<td>Product Name
			<td>Product Price
			<td>Action
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${channel}" var="product">
			<tr>
				<td>${product.product_name }</td>
				<td>RM ${product.product_unitprice }</td>
				<td>
					<a href="ProductController?action=updateProduct&id=${product.product_id }">Update</a>&nbsp;
					<a href="ProductController?action=deleteProduct&id=${product.product_id }">Delete</a>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<b>3)Services</b>
<a class="btn btn-primary" href="addServices.html">Add</a>
<br>
<table border=1>
	<thead>
		<tr>
			<td>Product Name
			<td>Product Price
			<td>Action
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${services}" var="product">
			<tr>
				<td>${product.product_name }</td>
				<td>RM ${product.product_unitprice }</td>
				<td>
					<a href="ProductController?action=updateProduct&id=${product.product_id }">Update</a>&nbsp;
					<a href="ProductController?action=deleteProduct&id=${product.product_id }">Delete</a>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>


</body>
</html>