<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Purchase Order</title>
<link rel="stylesheet" href="style.css">
</head>
<body>
<div class="topnav">
  <a class="active" href="customerindex.jsp">Home</a>
  <a href="ProductController?action=listCustomerProduct">Product</a>
  <a href="ProductController?action=listOrderProduct">Order Now</a>
  <div class="topnav-right">
    <div class="dropdown">
    <button class="dropbtn">My Account
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="AccountController?action=viewAccount"><i class="fa fa-user"></i>Profile</a>
      <a href="OrderController?action=listCustomerOrder"><i class="fa fa-shopping-cart"></i>Order</a>
      <a href="CheckoutController?action=view"><i class="fa fa-crosshairs"></i> Checkout</a>
      <a href="cart.jsp"  ><i class="fa fa-shopping-cart" ></i> Cart</a>
    </div>
  </div>
    <a href="LogoutController"><i class="fa fa-lock"></i> Logout</a>
  </div>
</div>

<h1>Product List by Type</h1>

<b>1)Subscription</b>
<form method="POST" action="CartController?action=add">
<table border=1>
	<thead>
		<tr>
			<td>Product Name
			<td>Product Type
			<td>Product Price
			<td>Quantity
			<td>Action
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${subscription}" var="product">
			<tr>
				<td>${product.product_name }</td>
				<td>${product.product_type }</td>
				<td>RM ${product.product_unitprice }</td>
				<td><input type="text" name="quantity"></td>
				<input type="hidden" value="${product.product_name }" name="product_name">
				<input type="hidden" value="${product.product_type }" name="product_type">
				<input type="hidden" value="${product.product_unitprice }" name="product_unitprice">
				<td><input type="submit" name="addToCart" value="Add To Cart" class="btn btn-default  add-to-cart"></td>
			</tr>
		</c:forEach>
	</tbody>
</table>
</form>
<b>2)Channel</b>
<form method="POST" action="CartController?action=add">
<table border=1>
	<thead>
		<tr>
			<td>Product Name
			<td>Product Type
			<td>Product Price
			<td>Action
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${channel}" var="product">
			<tr>
				<td>${product.product_name }</td>
				<td>${product.product_type }</td>
				<td>RM ${product.product_unitprice }</td>
				<td><input type="submit" name="addToCart" value="Add To Cart" class="btn btn-default  add-to-cart"></td>
			</tr>
		</c:forEach>
	</tbody>
</table>
</form>
<b>3)Services</b>
<form method="POST" action="CartController?action=add">
<table border=1>
	<thead>
		<tr>
			<td>Product Name
			<td>Product Type
			<td>Product Price
			<td>Action
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${services}" var="product">
			<tr>
				<td>${product.product_name }</td>
				<td>${product.product_type }</td>
				<td>RM ${product.product_unitprice }</td>
				<td><input type="submit" name="addToCart" value="Add To Cart" class="btn btn-default  add-to-cart"></td>
			</tr>
		</c:forEach>
	</tbody>
</table>
</form>
</body>
</html>